//#region Imports
import { Component, EventEmitter, Input, OnInit, OnDestroy, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { OutboundLien } from '../../model/OutboundLien';
import { GenerateLienModel } from '../../model/GenerateLienModel';
import { ResponseBase } from '../../model/ResponseBase';
import { Menu } from '../menu/menu';
import { TemplateDialog } from '../../model/templateDialog';
import { GrowlHelper } from '../../helper/growlHelper';
import { KeySet } from '../../model/keySet';
import {
    CheckboxModule,
    ConfirmDialogModule,
    ConfirmationService,
    DialogModule,
    DropdownModule,
    GrowlModule,
    InputTextModule,
    Message,
    MenuItem,
    MultiSelectModule,
    SelectItem,
    TreeModule,
    TreeNode,
    TooltipModule
} from 'primeng/primeng';

import * as moment from 'moment';
import * as $ from 'jquery';

import 'jquery.inputmask';
import 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import { LegalDataService } from '../../service/legalDataService';
import { OutboundLienTable } from './outBoundLienTable';
//#endregion Imports

@Component({
    selector: 'OutgoingClaimsTransmission',
    templateUrl: './app/component/outgoingClaimsTransmission/OutGoingClaimsTransmission.html',
    providers: [ConfirmationService]
})

export class OutgoingClaimsTransmission implements OnInit, OnDestroy {
    @ViewChild('outBoundLienTable') outBoundLienTable: OutboundLienTable;
    //Add inputs and outputs for your component
    @Output('onSaveComplete') onSaveComplete = new EventEmitter();
    @Input('externalKey') keyString: string;
    @Input('componentName') componentName: string;


    //#region Variables
    private sub: Subscription;
    private keySet: KeySet;
    private externalKey: number;
    private liens: OutboundLien[] = [];
    private parent: string = 'passedMessage';
    private lienSlice: OutboundLien[] = [];
    private lienTree: TreeNode[] = [];
    private selectedLienTree: TreeNode[] = [];
    private sortTreeOptions: SelectItem[];
    private selectedSortOption: string;
    private expandedOptions: SelectItem[] = [];
    private expanded: string;
    private topNode: TreeNode = [];
    private loading: TemplateDialog;
    private loadingTimeOffset: number;
    private treeCollapse: boolean = true;
    private generationResponse: ResponseBase
    private growlHelper: GrowlHelper;
    private validationOverride: boolean = false;
    private messages: Message[] = [];

    //#endregion Variables

    //#region Constructor
    constructor(private route: ActivatedRoute, private legalDataService: LegalDataService,
        private confirmationService: ConfirmationService, private router: Router) {
        this.loading = new TemplateDialog();
        this.loadingTimeOffset = 50;
        this.SetLoadingDisplay(true);
        this.sortTreeOptions = [];
        this.sortTreeOptions.push({ label: 'Admin', value: 'Admin' });
        this.sortTreeOptions.push({ label: 'Case', value: 'Case' });
        this.sortTreeOptions.push({ label: 'Client', value: 'Client' });
        this.selectedSortOption = 'Admin';
        this.expandedOptions.push({ label: 'Expanded', value: 'Exp' });
        this.expandedOptions.push({ label: 'Collapse', value: 'Col' });
        this.growlHelper = new GrowlHelper();

    }

    ngOnInit() {
        let key: string = this.keyString;
        let system: string;
        let component: string;
        let tagType: string;
        if (this.route.snapshot.url.length > 0 && this.route.snapshot.url[1].path === 'OutgoingClaimsTransmission') {
            this.sub = this.route
                .params
                .subscribe(params => {
                    key = params['key'];
                    component = params['component'];
                });

            if (key && component) {
                this.keySet = new KeySet(key);
                this.externalKey = this.keySet.PrimaryKey;
                this.componentName = component;
            }
        }
        this.legalDataService.getLiens()
            .then((liens: ResponseBase) => {

                if (liens.Data) {
                    this.liens = liens.Data;
                }
                if (!liens.Success) {
                    this.growlHelper.AddError(liens.Message);
                }
                this.lienTree.push(this.buildTree());
                this.buildTable();

            });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
    ///#endregion Constructor    

    //#region Events      
    private sortTreeOnChange(event: any) {
        this.SetLoadingDisplay(true);
        this.selectedLienTree = [];
        this.lienTree = [];
        this.lienTree.push(this.buildTree());
        this.buildTable();
    }

    private onNodeExpand(event: any) {
        if (event.node.label.substring(0, 3) === 'All') {
            this.expanded = 'Exp';
        }
    }

    private onNodeCollapse(event: any) {
        if (event.node.label.substring(0, 3) === 'All') {
            this.expanded = 'Col';
        }
    }

    private nodeSelect(event: any) {
        this.SetLoadingDisplay(true);
        setTimeout(() => {
            this.buildTable();
        }, this.loadingTimeOffset);
    }

    private nodeUnselect(event: any) {
        this.SetLoadingDisplay(true);
        setTimeout(() => {
            this.buildTable();
        }, this.loadingTimeOffset);
    }

    private onChange(event: any) {
        if (event.value === 'Exp') {
            this.expandRecursive(this.topNode, true);
        }
        else {
            this.expandRecursive(this.topNode, false);
        }
    }

    //#endregion Events

    //#region Methods
    private confirmGeneration(generatedLiens: OutboundLien[], generatePostData: GenerateLienModel) {
        let messageTemplate: string = null;
        if (generatePostData.Liens.length === 1) {
            messageTemplate = " liens will be generated. Please confirm."
        }
        else {
            messageTemplate = " liens will be generated. Please confirm."
        }
        let acceptanceTemplate: string = "You have accepted"
        this.confirmationService.confirm({
            message: generatePostData.Liens.length + messageTemplate,
            accept: () => {

                this.generationRequestSend(generatedLiens, generatePostData);
            }

        });
    }

    private generationRequestSend(generatedLiens: OutboundLien[], generatePostData: GenerateLienModel) {
        this.legalDataService.getGenerateOutboundClaimsTransmission(generatePostData)
            .then((response: ResponseBase) => {

                if (response.Success) {
                    this.growlHelper.AddInfo(response.Message);
                }
                else {
                    this.growlHelper.AddError(response.Message);
                }
            });


        generatedLiens.forEach((lien) => {
            var index = this.findLienIndex(this.liens, lien.SrsFileKey)
            if (index > -1) {
                this.liens.splice(index, 1);
            }
        })

        this.selectedLienTree = [];
        this.lienTree = [];
        this.lienTree.push(this.buildTree());
        this.buildTable();
    }

    private IsTreeCollapse() {
        return this.treeCollapse;
    }

    private SetLoadingDisplay(show: boolean) {
        if (show === true) {
            this.loading.Display = show;
        }
        else {
            setTimeout(() => {
                this.loading.Display = show;
            }, this.loadingTimeOffset);
        }
    }

    private expandRecursive(node: TreeNode, isExpand: boolean) {
        if (isExpand || (!isExpand && node.parent != undefined)) {
            node.expanded = isExpand;
        }
        if (node.children) {
            node.children.forEach(childNode => {
                this.expandRecursive(childNode, isExpand);
            });
        }
    }

    private buildTable() {
        this.lienSlice = [];
        if (this.liens.length !== 0) {
            this.selectedLienTree.forEach((x) => {
                if (x.children === undefined) {
                    x.data.forEach((y: any) => {
                        this.lienSlice.push(y);
                    })
                }
            })
        }
        this.SetLoadingDisplay(false);
    }

    private buildTree() {
        if (!this.liens) {
            return;
        }
        this.topNode = [];
        this.sortTree();

        this.liens.forEach((x) => {
            this.buildTreeRecurssive(x, this.topNode, 0);
        })

        this.setCounts(this.topNode);
        this.expandRecursive(this.topNode, true);
        this.expanded = 'Exp';
        return this.topNode;

    }

    private setCounts(node: TreeNode) {
        let count: number = 0;
        if (node.children === undefined) {
            if (this.liens.length === 0) {
                node.label = 'All (0)'
                return;
            }
            node.label = node.label.concat('(' + node.data.length + ')');
            return node.data.length;
        }
        node.children.forEach((x) => {
            count += this.setCounts(x);
        })
        node.label = node.label.concat('(' + count + ')');
        return count;
    }

    private buildTreeRecurssive(lien: OutboundLien, node: TreeNode, level: number) {
        let lienSelectorData: string = this.pullLienDataByTreeSelector(lien, level);
        if (node.data === undefined) {
            node.data = [];
        }
        if (level === 3) {
            node.data.push(lien);
            node.label = lienSelectorData;
            return;
        }
        node.label = lienSelectorData;

        if (node.children === undefined) {
            node.children = [];
        }
        let childSelectorData = this.pullLienDataByTreeSelector(lien, level + 1);
        let noMatch: boolean = false;
        node.children.forEach((c) => {
            // See if we match
            if (c.label.substring(0, childSelectorData.length) === childSelectorData) {
                this.buildTreeRecurssive(lien, c, level + 1);
                noMatch = true;
            }
        })
        // No Match
        if (!noMatch) {
            let newNode: TreeNode = [];
            node.children.push(newNode);
            this.buildTreeRecurssive(lien, newNode, level + 1);
        }
        return;
    }

    private pullLienDataByTreeSelector(lien: OutboundLien, level: number) {
        let data: string
        switch (this.selectedSortOption) {
            case 'Admin':
                switch (level) {
                    case 0:
                        data = 'All';
                        break;
                    case 1:
                        data = lien.AdminContact;
                        break;
                    case 2:
                        data = lien.Case;
                        break;
                    case 3:
                        data = lien.Project;
                        break;
                }
                break;
            case 'Case':
                switch (level) {
                    case 0:
                        data = 'All';
                        break;
                    case 1:
                        data = lien.Case;
                        break;
                    case 2:
                        data = lien.Project
                        break;
                    case 3:
                        data = lien.AdminContact;
                        break;
                }
                break;
            case 'Client':
                switch (level) {
                    case 0:
                        data = 'All';
                        break;
                    case 1:
                        data = lien.Client;
                        break;
                    case 2:
                        data = lien.Case;
                        break;
                    case 3:
                        data = lien.Project;
                }
                break;
        }
        return data;
    }

    private sortTree() {
        switch (this.selectedSortOption) {
            case 'Admin':
                this.liens.sort(this.adminSort);
                break;
            case 'Case':
                this.liens.sort(this.caseSort);
                break;
            case 'Client':
                this.liens.sort(this.clientSort);
                break;
        }
    }

    private caseSort(a: OutboundLien, b: OutboundLien) {
        if (a.Case < b.Case)
            return -1;
        if (a.Case > b.Case)
            return 1;
        return 0;
    }

    private adminSort(a: OutboundLien, b: OutboundLien) {
        if (a.AdminContact < b.AdminContact)
            return -1;
        if (a.AdminContact > b.AdminContact)
            return 1;
        return 0;
    }

    private clientSort(a: OutboundLien, b: OutboundLien) {
        if (a.Client < b.Client)
            return -1;
        if (a.Client > b.Client)
            return 1;
        return 0;
    }

    private onGenerateClick(generatedLiens: OutboundLien[]) {
        let generatePostData: GenerateLienModel = new GenerateLienModel();
        generatePostData.Liens = generatedLiens;
        generatePostData.IsOverride = this.validationOverride;

        //Show Modal
        this.confirmGeneration(generatedLiens, generatePostData);
        return;
    }

    private findLienIndex(liens: OutboundLien[], srsfilekey: number): number {
        let indexMatch: number = -1;

        liens.forEach(function (lien, index) {
            if (lien.SrsFileKey === srsfilekey) {
                indexMatch = index;
            }
        }
        )

        return indexMatch;

    }

    //#endregion Methods
}
