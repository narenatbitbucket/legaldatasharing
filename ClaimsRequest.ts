//#region Imports
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Menu } from '../menu/menu';
import { GrowlHelper } from '../../helper/growlHelper';

import { TemplateDialog } from '../../model/templateDialog';
import { ClaimRequest } from '../../model/ClaimRequest';
import { GenerateClaimRequestModel } from '../../model/GenerateClaimRequestModel';
import { ResponseBase } from '../../model/ResponseBase';
import { RawlingsPermission } from '../../model/rawlingsPermission';
import { PermissionState } from '../../model/rawlingsPermission';
import { LegalDataService } from '../../service/legalDataService';

import {
    CheckboxModule,
    ConfirmDialogModule,
    ConfirmationService,
    DialogModule,
    DropdownModule,
    GrowlModule,
    InputTextModule,
    Message,
    MenuItem,
    MultiSelectModule,
    SelectItem,
    TreeModule,
    TreeNode,
    TooltipModule
} from 'primeng/primeng';

import * as moment from 'moment';
import * as $ from 'jquery';
import 'jquery.inputmask';
import 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import { ClaimRequestTable } from './ClaimRequestTable';
//#endregion Imports


@Component({
    selector: 'ClaimsRequest',
    templateUrl: './app/component/claimsRequest/ClaimsRequest.html'
})

export class ClaimsRequest implements OnInit {
    @ViewChild('ClaimRequestTable') ClaimRequestTable: ClaimRequestTable;
    @ViewChild('childAccess') childAccess: ClaimRequestTable
    //#region Variables
    private growlHelper: GrowlHelper;
    private loading: TemplateDialog;
    private loadingTimeOffset: number;

    private generating: TemplateDialog;
    private generatingTimeOffset: number;

    private claimRequests: ClaimRequest[] = [];
    private claimRequestSlice: ClaimRequest[] = [];
    private claimRequestTree: TreeNode[] = [];
    private selectedClaimRequestTree: TreeNode[] = [];
    private claimRequestData: ClaimRequest[] = [];
  
    private sortTreeOptions: SelectItem[];
    private selectedSortOption: string;
    private expandedOptions: SelectItem[] = [];
    private expanded: string;

    //private treeCollapse: boolean = true;
    private topNode: TreeNode = [];

    private validationOverride: boolean = false;
    private messages: Message[] = [];

    private get stickyGrowl(): boolean {
        let ret: boolean = false;

        this.messages.forEach(it => {
            if (it.severity.toUpperCase() === 'ERROR') {
                ret = true;

                return;
            }
        });

        return ret;
    }

    private requestTypeOptions: any[] = [];
    private requestType: string;
    private filteredClaimRequests: ClaimRequest[] = [];

    private dataKey: string;
    private dataLoadCount: number;
    private startIndex: number;

    private dataToSend: any[];
    private dataBatch: ClaimRequest[];
    private totalCount: number;
    private sendIndex: number;
    private generatePermission: RawlingsPermission = new RawlingsPermission();

    //#endregion Variables

    //#region Constructor
    constructor(private route: ActivatedRoute, private legalDataService: LegalDataService,
        private confirmationService: ConfirmationService, private router: Router) {
        this.loading = new TemplateDialog();
        this.loadingTimeOffset = 50;

        this.generating = new TemplateDialog();
        this.generatingTimeOffset = 50;

        this.SetLoadingDisplay(true);
        this.sortTreeOptions = [];
        this.sortTreeOptions.push({ label: 'By Case', value: 'Case' });
        this.sortTreeOptions.push({ label: 'By Client', value: 'Client' });
        this.selectedSortOption = 'Client';

        this.expandedOptions.push({ label: 'Expanded', value: 'Exp' });
        this.expandedOptions.push({ label: 'Collapse', value: 'Col' });

        this.requestTypeOptions.push({ label: 'Rep', value: 'Rep' });
        this.requestTypeOptions.push({ label: 'System', value: 'System' });
        this.requestTypeOptions.push({ label: 'All', value: 'All' });

        this.growlHelper = new GrowlHelper();
    }

    ngOnInit() {
        this.generatePermission.ModuleName = "ClaimRequest";
        this.generatePermission.ViewTemplate = "SendClaimData";
        this.generatePermission.ReadWrite = PermissionState.Write;

        this.legalDataService.getPermissions(this.generatePermission)
            .then((permissionSet: ResponseBase) => {

                if (permissionSet.Data) {
                    this.childAccess.SetPermission(permissionSet.Data[0]);                    
                }
                else {
                    this.growlHelper.AddInfo(permissionSet.Message);
                }
            });

        this.legalDataService.getClaimRequests()
            .then((data: any[]) => {
                this.dataKey = data[2];
                if (data[0] != null && data[0] > 0) {
                    this.setClaimRequestDataSize(data[0]);
                }
                else {
                    this.SetLoadingDisplay(false);
                }

                if (data.length > 1 && data[1] != null && data[1].type != undefined && data[1].type.toUpperCase() === "DANGER") {
                    this.growlHelper.AddError(data[1].message);
                    this.SetLoadingDisplay(false);
                    return;
                }

            });
    }


    //#endregion Constructor    


    //region Methods

    private setClaimRequestDataSize(size: number) {
        this.dataLoadCount = size;

        if (this.dataLoadCount > 0) {
            this.startIndex = 0;

            //These calls makes 10 parallel calls to speed up the retrieval
            this.requestDataBatch();
            this.requestDataBatch();
            this.requestDataBatch();
            this.requestDataBatch();
            this.requestDataBatch();
            this.requestDataBatch();
            this.requestDataBatch();
            this.requestDataBatch();
            this.requestDataBatch();
            this.requestDataBatch();
        }
    }

    private requestDataBatch() {
        if (this.startIndex < this.dataLoadCount) {
            var param = {
                Guid: this.dataKey,
                StartIndex: this.startIndex,
                BatchSize: 1000
            };

            this.legalDataService.getClaimRequestBatch(param)
                .then((data: any[]) => {
                    this.dataKey = data[2];
                    this.setClaimRequestData(data[0]);

                    if (data.length > 1 && data[1] != null && data[1].type != undefined && data[1].type.toUpperCase() === "DANGER") {
                        this.growlHelper.AddError(data[1].message);
                        this.SetLoadingDisplay(false);
                        return;
                    }
                });
            this.startIndex += param.BatchSize;
        }
    }

    private setClaimRequestData(data: any[]) {
        if (data != null) {

            this.claimRequestData = this.claimRequestData.concat(data);

            if (this.claimRequestData.length >= this.dataLoadCount) {
                var param = {
                    Guid: this.dataKey,
                    StartIndex: -1,
                    BatchSize: -1
                };

                this.legalDataService.claimRequestDone(param);

                this.setClaimRequestDataFinal();
            } else {
                this.requestDataBatch();
            }
        }
    }

    private setClaimRequestDataFinal() {
        if (this.claimRequestData != null) {
            this.claimRequests = this.claimRequestData;
            this.filteredClaimRequests = this.claimRequestData;
        }

        this.claimRequestTree.push(this.buildTree(this.claimRequests));
        this.buildTable(this.claimRequests);
    }

    private buildTable(requests: ClaimRequest[]) {
        this.claimRequestSlice = [];
        if (requests.length !== 0) {
            this.selectedClaimRequestTree.forEach((x) => {
                if (x.children === undefined) {
                    x.data.forEach((y: any) => {
                        this.claimRequestSlice.push(y);
                    })
                }
            })
        }
        this.SetLoadingDisplay(false);
    }

    private buildTree(requests: ClaimRequest[]) {
        if (!requests) {
            return;
        }
        this.topNode = [];
        this.sortTree();

        requests.forEach((x) => {
            this.buildTreeRecurssive(x, this.topNode, 0);
        })

        this.setCounts(this.topNode, requests);
        this.expandRecursive(this.topNode, true);
        this.expanded = 'Exp';
        return this.topNode;

    }

    private setCounts(node: TreeNode, request: ClaimRequest[]) {
        let count: number = 0;
        if (node.children === undefined) {
            if (request.length === 0) {
                node.label = 'All (0)'
                return;
            }
            node.label = node.label.concat('(' + node.data.length + ')');
            return node.data.length;
        }
        node.children.forEach((x) => {
            count += this.setCounts(x, request);
        })
        node.label = node.label.concat('(' + count + ')');
        return count;
    }

    private buildTreeRecurssive(claimRequest: ClaimRequest, node: TreeNode, level: number) {
        let requestSelectorData: string = this.pullClaimRequestDataByTreeSelector(claimRequest, level);
        if (node.data === undefined) {
            node.data = [];
        }
        if (level === 3) {
            node.data.push(claimRequest);
            node.label = requestSelectorData;
            return;
        }
        node.label = requestSelectorData;

        if (node.children === undefined) {
            node.children = [];
        }
        let childSelectorData = this.pullClaimRequestDataByTreeSelector(claimRequest, level + 1);
        let noMatch: boolean = false;
        node.children.forEach((c) => {
            // See if we match
            if (c.label.substring(0, childSelectorData.length) === childSelectorData) {
                this.buildTreeRecurssive(claimRequest, c, level + 1);
                noMatch = true;
            }
        })
        // No Match
        if (!noMatch) {
            let newNode: TreeNode = [];
            node.children.push(newNode);
            this.buildTreeRecurssive(claimRequest, newNode, level + 1);
        }
        return;
    }

    private pullClaimRequestDataByTreeSelector(request: ClaimRequest, level: number) {
        let data: string
        switch (this.selectedSortOption) {
            case 'Case':
                switch (level) {
                    case 0:
                        data = 'All';
                        break;
                    case 1:
                        data = request.CaseName;
                        break;
                    case 2:
                        data = request.ClientCode;
                        break;
                    case 3:
                        data = request.ProjectName;
                        break;
                }
                break;
            case 'Client':
                switch (level) {
                    case 0:
                        data = 'All';
                        break;
                    case 1:
                        data = request.ClientCode;
                        break;
                    case 2:
                        data = request.CaseName;
                        break;
                    case 3:
                        data = request.ProjectName;
                        break;
                }
                break;
        }
        return data;
    }

    private sortTree() {
        switch (this.selectedSortOption) {
            case 'Case':
                this.claimRequests.sort(this.caseSort);
                break;
            case 'Client':
                this.claimRequests.sort(this.clientSort);
                break;
        }
    }

    private caseSort(a: ClaimRequest, b: ClaimRequest) {
        if (a.CaseName < b.CaseName)
            return -1;
        if (a.CaseName > b.CaseName)
            return 1;
        return 0;
    }

    private clientSort(a: ClaimRequest, b: ClaimRequest) {
        if (a.ClientCode < b.ClientCode)
            return -1;
        if (a.ClientCode > b.ClientCode)
            return 1;
        return 0;
    }

    private expandRecursive(node: TreeNode, isExpand: boolean) {
        if (isExpand || (!isExpand && node.parent != undefined)) {
            node.expanded = isExpand;
        }
        if (node.children) {
            node.children.forEach(childNode => {
                this.expandRecursive(childNode, isExpand);
            });
        }
    }

    private onGenerateClick(generatedClaimRequests: ClaimRequest[]) {
        this.confirmGeneration(generatedClaimRequests);

        return;
    }

    private confirmGeneration(generatedClaimRequests: ClaimRequest[]) {
        let messageTemplate: string = null;

        if (generatedClaimRequests.length > 0) {
            messageTemplate = generatedClaimRequests.length + " Claim Requests will be generated. Please confirm."

            this.confirmationService.confirm({
                message: messageTemplate,
                accept: () => {
                    this.generationRequestSend(generatedClaimRequests);
                }

            });
        }
    }

    private generationRequestSend(generatedClaimRequests: ClaimRequest[]) {
        this.SetGeneratingDisplay(true);
        this.dataToSend = [];
        var dataBatch: ClaimRequest[] = [];

        var batchSize = 1000;
        var index = 0;
        this.totalCount = generatedClaimRequests.length;

        for (var i = 0; i < generatedClaimRequests.length; i++) {
            dataBatch.push(generatedClaimRequests[i]);

            if (dataBatch.length >= batchSize) {
                this.dataToSend.push(dataBatch);
                dataBatch = [];
            }
        }

        if (dataBatch.length > 0) {
            this.dataToSend.push(dataBatch);
            dataBatch = [];
        }

        this.sendIndex = 0;
        this.sendDataBatch(generatedClaimRequests);

    }

    private sendDataBatch(generatedClaimRequests: ClaimRequest[]) {
        if (this.sendIndex < this.dataToSend.length) {
            var param = {
                Guid: this.dataKey,
                TotalCount: this.totalCount,
                Data: this.dataToSend[this.sendIndex]
            };

            this.legalDataService.sendClaimData(param)
                .then((response: any) => {

                    if (response[0] === null && response[1] === null) {
                        this.sendIndex++;
                        this.sendDataBatch(generatedClaimRequests);
                    }
                    else {
                        this.growlHelper.AddError(response[1].message);
                        this.SetLoadingDisplay(false);
                        return;
                    }
                });
        }
        else {
            this.sendClaimRequests(generatedClaimRequests);
        }

    }

    private sendClaimRequests(generatedClaimRequests: ClaimRequest[]) {
        var param = {
            Guid: this.dataKey
        };

        this.legalDataService.sendClaimRequests(param)
            .then((response: any) => {

                //Hide the Generating dialog
                this.SetGeneratingDisplay(false);

                if (response[1] != null
                    && response[1] != undefined
                    && response[1].type != undefined
                    && response[1].type.toUpperCase() === "SUCCESS") {
                    this.growlHelper.AddInfo(response[1].message);
                }
                else if (response[1] != null
                    && response[1] != undefined
                    && response[1].type != undefined
                    && response[1].type.toUpperCase() === "DANGER") {
                    this.growlHelper.AddError(response[1].message);
                    return;
                }
            });

        generatedClaimRequests.forEach((request) => {
            var index = this.findClaimRequestIndex(this.claimRequests, request.PlaintiffListItemId, request.ClientCodeId)
            if (index > -1) {
                this.claimRequests.splice(index, 1);
            }
        })

        this.dataToSend = [];
        this.totalCount = 0;
        this.selectedClaimRequestTree = [];
        this.claimRequestTree = [];
        this.claimRequestTree.push(this.buildTree(this.claimRequests));
        this.buildTable(this.claimRequests);
        this.requestType = "All";
    }

    private findClaimRequestIndex(claimRequests: ClaimRequest[], plaintiffListItemId: number, clientCodeId: number): number {
        let indexMatch: number = -1;

        claimRequests.forEach(function (claimRequest, index) {
            if (claimRequest.PlaintiffListItemId === plaintiffListItemId && claimRequest.ClientCodeId === clientCodeId) {
                indexMatch = index;
            }
        })

        return indexMatch;
    }

    //$endregion Methods 


    //#region Events   

    private sortTreeOnChange(event: any) {
        this.SetLoadingDisplay(true);
        this.selectedClaimRequestTree = [];
        this.claimRequestTree = [];
        this.claimRequestTree.push(this.buildTree(this.filteredClaimRequests));
        this.buildTable(this.filteredClaimRequests);
    }

    private onNodeExpand(event: any) {
        if (event.node.label.substring(0, 3) === 'All') {
            this.expanded = 'Exp';
        }
    }

    private onNodeCollapse(event: any) {
        if (event.node.label.substring(0, 3) === 'All') {
            this.expanded = 'Col';
        }
    }

    private nodeSelect(event: any) {
        this.SetLoadingDisplay(true);
        setTimeout(() => {
            this.buildTable(this.filteredClaimRequests);
        }, this.loadingTimeOffset);
    }

    private nodeUnselect(event: any) {
        this.SetLoadingDisplay(true);
        setTimeout(() => {
            this.buildTable(this.filteredClaimRequests);
        }, this.loadingTimeOffset);
    }

    private onChange(event: any) {
        if (event.value === 'Exp') {
            this.expandRecursive(this.topNode, true);
        }
        else {
            this.expandRecursive(this.topNode, false);
        }
    }

    private onRequestTypeChange(event: any) {
        this.topNode = [];
        this.filteredClaimRequests = [];

        if (this.claimRequests.length !== 0) {
            this.SetLoadingDisplay(true);

            setTimeout(() => {
                if (this.requestType === 'Rep') {
                    this.claimRequests.forEach((x) => {
                        if (x.RepRequest === true) {
                            this.filteredClaimRequests.push(x);
                        }
                    });
                }
                else if (this.requestType === 'System') {
                    this.claimRequests.forEach((x) => {
                        if (x.RepRequest === false) {
                            this.filteredClaimRequests.push(x);
                        }
                    });
                }
                else {
                    this.claimRequests.forEach((x) => {
                        this.filteredClaimRequests.push(x);

                    });
                }

                this.selectedClaimRequestTree = [];
                this.claimRequestTree = [];
                this.claimRequestTree.push(this.buildTree(this.filteredClaimRequests));
                this.buildTable(this.filteredClaimRequests);
            }, this.loadingTimeOffset);

            this.SetLoadingDisplay(false);
        }


    }

    private SetLoadingDisplay(show: boolean) {
        if (show === true) {
            this.loading.Display = show;
        }
        else {
            setTimeout(() => {
                this.loading.Display = show;
            }, this.loadingTimeOffset);
        }
    }

    private SetGeneratingDisplay(show: boolean) {
        if (show === true) {
            this.generating.Display = show;
        }
        else {
            setTimeout(() => {
                this.generating.Display = show;
            }, this.generatingTimeOffset);
        }
    }

    //#endregion Events  
}
