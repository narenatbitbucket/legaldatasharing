//#region Imports
import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ClaimRequest } from '../../model/ClaimRequest';
import { GridPaging } from '../SharedComponents/GridPaging';

import moment from 'moment';
import * as $ from 'jquery';
import { Observable } from 'rxjs/Rx';
import { Helper } from '../../helper/helper';
import { GridHelper } from '../../helper/gridHelper';
import { GlobalFilter } from '../../helper/globalFilter';
import { GridColumn } from '../../model/gridColumn';

import * as wjcCore from 'wijmo/wijmo';
import * as wjcGrid from 'wijmo/wijmo.grid';
import * as wjcGridFilter from 'wijmo/wijmo.grid.filter'

import {
     Message
    , SelectItem
    , MultiSelect
    , Footer
    , ListboxModule
    , CheckboxModule
    , InputTextModule
    , TooltipModule
} from 'primeng/primeng';

import 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import { LegalDataService } from '../../service/legalDataService';
import { ConfirmDialogModule, ConfirmationService } from 'primeng/primeng';
import 'jquery.inputmask';
//#endregion Imports

@Component({
    selector: 'ClaimRequestTable',
    templateUrl: './app/component/claimsRequest/ClaimRequestTable.html'
})

export class ClaimRequestTable implements OnInit {
    @Input() claimRequestSlice: ClaimRequest[] = [];

    @ViewChild('grid') grid: wjcGrid.FlexGrid;

    @Output() onGenerateClick: EventEmitter<any> = new EventEmitter();

    //#region Variables    
    private gridHelper: GridHelper;
    private generationClaimRequestSlice: ClaimRequest[] = [];
    private actualClaimRequestSlice: ClaimRequest[] = [];
    private generationCount: number;
    private pageSize: number;
    private hasGeneratePermission = false;

    //#endregion Variables


    //#region Constructor
    constructor(private route: ActivatedRoute, private legalDataService: LegalDataService, private router: Router, private confirmationService: ConfirmationService) {
        this.pageSize = 50;
    }

    ngOnInit() {
        this.setGridColumns();

        this.gridHelper.initialize();

        this.setGridData();

    }

    //#endregion Constructor 


    //#region Events

    private ngOnChanges(args: any) {
        if (args.claimRequestSlice !== undefined) {
            this.updateActualClaimRequestSlice();
            this.setGridData();
        }
    }

    //EndRegion

    //region methods

    public SetPermission(value: boolean)
    {
        this.hasGeneratePermission = value;
    }

    private setGridColumns() {
        this.gridHelper = new GridHelper('PlaintiffListTable', this.grid);
        
        this.gridHelper.cols = [
            { Field: 'SendIt', Header: 'Send It', AllowToggle: false, Width: 85 },
            { Field: 'RepRequest', Header: 'Rep Request', AllowToggle: true, Width: 100 },
            { Field: 'PlaintiffListItemId', Header: 'PLI Id', AllowToggle: true, Width: 105 },
            { Field: 'ClientCode', Header: 'Client Code', AllowToggle: false, Width: 248 },
            { Field: 'CaseName', Header: 'Case Name', AllowToggle: false, Width: 248 },
            { Field: 'ProjectName', Header: 'Project Name', AllowToggle: false, Width: 248 },
            { Field: 'FirstName', Header: 'First Name', AllowToggle: true, Width: 115 },
            { Field: 'LastName', Header: 'Last Name', AllowToggle: true, Width: 115 },
            { Field: 'Ssn', Header: 'SSN', AllowToggle: true, Width: 110 },
            { Field: 'Dob', Header: 'DOB', AllowToggle: true, Width: 110 },
            { Field: 'AdminNo', Header: 'Admin No', AllowToggle: true, Width: 120 },
            { Field: 'PlaintiffListId', Header: 'PL Id', AllowToggle: true, Width: 148 },
            { Field: 'PlaintiffListDate', Header: 'PlaintiffListDate', AllowToggle: true, Width: 130 },
            { Field: 'IsActive', Header: 'Is Active', AllowToggle: true, Width: 100 },
            { Field: 'HasMatches', Header: 'Has Matches', AllowToggle: true, Width: 180 },
            { Field: 'DataCount', Header: 'Data Count', AllowToggle: true, Width: 180 },
        ]
    }

    private setGridData() {
        setTimeout(() => {
            this.gridHelper.wData = new wjcCore.CollectionView(this.actualClaimRequestSlice);
            this.gridHelper.wData.pageSize = this.pageSize;
            this.gridHelper.globalFilter = new GlobalFilter(this.gridHelper.wData, undefined, () => {
                this.onFilterComplete();
            });
        }, 10);
    }

    private onFilterComplete() {
        setTimeout(() => {
            this.gridHelper.autoSizeRows();
        }, 10);
    }

    private onColumnOptionsChange() {
        this.gridHelper.toggleColumn();
    }

    private parentPageSizeChange(selectedPageSize: number) {
        this.setGridData();
    }

    confirm() {
        this.ResetGenerationClaimRequestSlice();

        this.GenerateSelection();
    }

    private ResetGenerationClaimRequestSlice() {
        this.generationClaimRequestSlice = [];

        if (this.actualClaimRequestSlice !== undefined && this.actualClaimRequestSlice !== null) {
            this.actualClaimRequestSlice.forEach((x) => {
                if (x.SendIt) {
                    this.generationClaimRequestSlice.push(x);
                }
            })
        }

        this.generationCount = this.generationClaimRequestSlice.length;

    }

    private GetRecordsToSendCount(grid: wjcGrid.FlexGrid) {
        let count: number = 0;
        if (this.actualClaimRequestSlice !== undefined && this.actualClaimRequestSlice !== null) {
            this.actualClaimRequestSlice.forEach((x) => {
                if (x.SendIt) {
                    count++;
                }
            })
        }
        return count;
    }

    GenerateSelection() {

        if (this.generationClaimRequestSlice.length > 0) {
            this.onGenerateClick.emit(this.generationClaimRequestSlice);
        }

    }

    private GetUnFilteredRecordCount(grid: wjcGrid.FlexGrid) {
        let count: number = 0;

        if (this.gridHelper.wData) {
            count = this.gridHelper.wData.totalItemCount;
        }

        return count;
    }

    private export() {
        var csv = '';
        var csvData = 'data:text/csv;charset=utf-8,';

        let header: string = '';
        let body: string = '';

        //headers
        for (var i = 0; i < this.gridHelper.cols.length; i++) {
            if (this.gridHelper.cols[i].Header && this.gridHelper.cols[i].Field.toUpperCase() != 'SENDIT') {
                if (header.length > 0) header += ',';//this.dt.csvSeparator;

                header += this.gridHelper.cols[i].Header;
            }
        }

        csv += header;

        //body        
        this.actualClaimRequestSlice.forEach(it => {
            csv += '\r\n';
            body = '';

            for (var i = 0; i < this.gridHelper.cols.length; i++) {
                if (this.gridHelper.cols[i].Header && this.gridHelper.cols[i].Field.toUpperCase() != 'SENDIT') {
                    let value: any = it[this.gridHelper.cols[i].Field];
                    if (typeof value === 'string') {
                        value = '"' + value.replace(/\"/g, '""') + '"';
                    }

                    if (body.length > 0) body += ',';//this.dt.csvSeparator;

                    body += value;
                }
            }

            csv += body;
        });

        var fileName = 'download.csv';
        var link = document.createElement('a');
        link.setAttribute('href', csvData + encodeURI(csv));
        link.setAttribute('download', fileName);
        link.click();
    };

    private get checkedClaimRequestCount(): number {
        let ret: number = 0;

        if (this.actualClaimRequestSlice !== undefined && this.actualClaimRequestSlice !== null) {
            this.actualClaimRequestSlice.forEach((x) => {
                if (x.SendIt) {
                    ret = ret + 1;
                }
            })
        }

        return ret;
    }

    private updateActualClaimRequestSlice() {
        this.SelectiveDeepCopy(this.claimRequestSlice, this.actualClaimRequestSlice);
    }

    private SelectiveDeepCopy(arrayFrom: any, arrayTo: any) {
        let i: number = arrayTo.length; // arrayTo iteration
        let j: number = arrayFrom.length; // arrayFrom iteration
        let k: number = 0; //the number of items already removed

        for (let x: number = 0; x < i; x++) { // Loop i -1 times
            let index = -1; // default to not found
            arrayFrom.forEach((af: any) => { // loop through arrayFrom to find  if array[x] SrsFileKey matches
                if (af.PlaintiffListItemId === arrayTo[x - k].PlaintiffListItemId && af.ClientCodeId === arrayTo[x - k].ClientCodeId) { //Check each item in arrayFrom to see if it already contains the PlaintiffListItemId and ClientCodeId with an offset for what we have removed in k
                    index = 0; // if it does, no need to remove it
                }
            });
            if (index === -1) { // if missing
                arrayTo.splice(x - k, 1); // k is the number of items already removed
                k++; // increment the number of items removed
            }
        };

        for (let y: number = 0; y < j; y++) { // same as above, but we are moving through arrayFrom and if arrayTo is missing the PlaintiffListItemId and ClientCodeId, we are pushing it on the arrayTo
            let index: number = -1;
            arrayTo.forEach((at: any) => {
                if (at.PlaintiffListItemId === arrayFrom[y].PlaintiffListItemId && at.ClientCodeId === arrayFrom[y].ClientCodeId) {
                    index = 0;
                }
            });
            if (index === -1) {
                arrayTo.push(Object.assign({}, arrayFrom[y]));
            }
        };
    }

    private SendIt(value: boolean) {
        if (this.gridHelper.wData) {
            this.gridHelper.wData._view.forEach((x) => {
                x.SendIt = value;
            })
        }
        else {
            this.actualClaimRequestSlice.forEach((x) => {
                x.SendIt = value;
            })
        }
    }

    //endregion



}